class CreateTaggedSkills < ActiveRecord::Migration
  def change
    create_table :tagged_skills do |t|
      t.integer :skill_id
      t.integer :user_id
      t.string :state

      t.timestamps null: false
    end
    add_index :tagged_skills, [:skill_id, :user_id], unique: true
  end
end
