class CreateSkillPlus < ActiveRecord::Migration
  def change
    create_table :skill_plus do |t|
      t.integer :skill_id
      t.integer :tagged_user_id
      t.integer :by_user_id

      t.timestamps null: false
    end
    add_index :skill_plus, :skill_id
    add_index :skill_plus, :tagged_user_id
    add_index :skill_plus, :by_user_id
    add_index :skill_plus, [:skill_id, :tagged_user_id, :by_user_id], unique: true

  end
end
