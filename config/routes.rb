Rails.application.routes.draw do

  resources :tagged_skills,    :only => [:create, :destroy, :publish_skill, :hide_skill]
  resources :skill_plus,       :only => [:create, :destroy]
  resources :skills,           :only => [:index, :show, :create]
  resources :users            

  root 'application#root'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  post   'publish_skill'  => 'tagged_skills#publish_skill'
  post   'hide_skill'     => 'tagged_skills#hide_skill'

end
