class TaggedSkill < ActiveRecord::Base
  belongs_to :user, 	  class_name: "User"
  belongs_to :skill, 	  class_name: "Skill"
  
  validates :user, presence: true
  validates :skill, presence: true
end
