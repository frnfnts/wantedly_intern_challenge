class Skill < ActiveRecord::Base
	has_many :skill_plus,
		class_name: "SkillPlu",
		foreign_key: "skill_id",
		dependent: :destroy
	has_many :tagged_user,
		class_name: "TaggedSkill",
		foreign_key: "skill_id",
		dependent: :destroy
end
