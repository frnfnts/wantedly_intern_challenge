class User < ActiveRecord::Base
	has_many :active_skill_plus,
		class_name: "SkillPlu",
		foreign_key: "by_user_id",
		dependent: :destroy
	has_many :passive_skill_plus,
		class_name: "SkillPlu",
		foreign_key: "tagged_user_id",
		dependent: :destroy

	has_many :tagged_skills,
		class_name: "TaggedSkill",
		foreign_key: "user_id",
		dependent: :destroy

	has_many :plus_skills,
		through: :passive_skill_plus,
		source: :skill
end
