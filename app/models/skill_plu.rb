class SkillPlu < ActiveRecord::Base
  belongs_to :tagged_user, class_name: "User"
  belongs_to :by_user,     class_name: "User"
  belongs_to :skill, 	  class_name: "Skill"
  
  validates :tagged_user, presence: true
  validates :by_user, presence: true
  validates :skill, presence: true
end
