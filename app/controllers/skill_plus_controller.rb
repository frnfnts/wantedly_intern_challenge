class SkillPlusController < ApplicationController
  include SkillPlusHelper
  before_action :set_skill_plu, only: [:show, :edit, :update, :destroy]

  # GET /skill_plus
  # GET /skill_plus.json
  def index
    @skill_plus = SkillPlu.all
  end

  # GET /skill_plus/1
  # GET /skill_plus/1.json
  def show
  end

  # GET /skill_plus/new
  def new
    @skill_plu = SkillPlu.new
  end

  # GET /skill_plus/1/edit
  def edit
  end

  # POST /skill_plus
  # POST /skill_plus.json
  def create
    if params[:tagged_user_id] == params[:by_user_id]
      redirect_to :back
      return
    end
    if not is_plused?(*skill_plu_params)
      @skill_plu = SkillPlu.create(skill_plu_params)
    end
    redirect_to :back
  end

  # PATCH/PUT /skill_plus/1
  # PATCH/PUT /skill_plus/1.json
  def update
    respond_to do |format|
      if @skill_plu.update(skill_plu_params)
        format.html { redirect_to @skill_plu, notice: 'Skill plu was successfully updated.' }
        format.json { render :show, status: :ok, location: @skill_plu }
      else
        format.html { render :edit }
        format.json { render json: @skill_plu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /skill_plus/1
  # DELETE /skill_plus/1.json
  def destroy
    @skill_plu.destroy
    redirect_to :back
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_skill_plu
      @skill_plu = SkillPlu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def skill_plu_params
      params.permit(:skill_id, :tagged_user_id, :by_user_id)
    end
end
