class TaggedSkillsController < ApplicationController
  before_action :set_tagged_skill, only: [:show, :edit, :update, :destroy]

  # GET /tagged_skills
  # GET /tagged_skills.json
  def index
    @tagged_skills = TaggedSkill.all
  end

  # GET /tagged_skills/1
  # GET /tagged_skills/1.json
  def show
  end

  # GET /tagged_skills/new
  def new
    @tagged_skill = TaggedSkill.new
  end

  # GET /tagged_skills/1/edit
  def edit
  end

  # POST /tagged_skills
  # POST /tagged_skills.json
  def create
    params[:tagged_skill][:skill_id] = Skill.find_or_create_by(name: params[:tagged_skill][:skill_id]).id
    @tagged_skill = TaggedSkill.find_or_create_by(tagged_skill_params)
    redirect_to :back

  end

  # PATCH/PUT /tagged_skills/1
  # PATCH/PUT /tagged_skills/1.json
  def update
    respond_to do |format|
      if @tagged_skill.update(tagged_skill_params)
        format.html { redirect_to @tagged_skill, notice: 'Tagged skill was successfully updated.' }
        format.json { render :show, status: :ok, location: @tagged_skill }
      else
        format.html { render :edit }
        format.json { render json: @tagged_skill.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tagged_skills/1
  # DELETE /tagged_skills/1.json
  def destroy
    @tagged_skill.destroy
    respond_to do |format|
      format.html { redirect_to tagged_skills_url, notice: 'Tagged skill was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def publish_skill
    p = params
    if p[:user_id].to_i != current_user.id
      redirect_to :back
      return
    end
    ts = TaggedSkill.find_by(skill_id: p[:skill_id], user_id: p[:user_id])
    ts.state = "public"
    ts.save
    redirect_to :back
  end

  def hide_skill
    p = params
    if p[:user_id].to_i != current_user.id
      redirect_to :back
      return
    end
    ts = TaggedSkill.find_by(skill_id: p[:skill_id], user_id: p[:user_id])
    ts.state = "private"
    ts.save
    redirect_to :back
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tagged_skill
      @tagged_skill = TaggedSkill.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tagged_skill_params
      params.require(:tagged_skill).permit(:skill_id, :user_id, :state)
    end
end
