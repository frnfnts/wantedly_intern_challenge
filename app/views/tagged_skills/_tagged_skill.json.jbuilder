json.extract! tagged_skill, :id, :skill_id, :user_id, :state, :created_at, :updated_at
json.url tagged_skill_url(tagged_skill, format: :json)
