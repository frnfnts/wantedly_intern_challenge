json.extract! skill_plu, :id, :skill_id, :tagged_user_id, :by_user_id, :created_at, :updated_at
json.url skill_plu_url(skill_plu, format: :json)
