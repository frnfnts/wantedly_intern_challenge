module UsersHelper
	def get_skill_counts(user, state="public")
		# skill id, user id ,state
		tss = user.tagged_skills.where(state: state)  
		# skill id, user id(tagged), user id(who plussed)
		psps = user.passive_skill_plus.select("id, tagged_user_id, by_user_id AS byu, skill_id AS skid")
		psps = tss.joins("LEFT JOIN (#{psps.to_sql}) AS X ON user_id = tagged_user_id AND tagged_skills.\"skill_id\" = skid") 
		# skill id, count
		psps = psps.select('tagged_skills."skill_id", COUNT(byu) AS cnt')
			.group('tagged_skills."skill_id"') 
		# skill id, count, skill name
		psps = ActiveRecord::Base.connection.select_all(
			"SELECT skill_id, cnt, name
				FROM (#{psps.to_sql}) AS Y LEFT JOIN skills ON skill_id = skills.id")
		tss = psps.map{|x| x["skill_id"]} 
		counts = psps.map{|x| x["cnt"]}
		skill_names = psps.map{|x| x["name"]}
		counts = tss.zip(counts, skill_names).sort{|a, b| b[1] <=> a[1]}
	end


	def get_plussed_skills(curren_user, user, counts)
		plussed_skills = current_user.active_skill_plus
		    .where("by_user_id = ? AND tagged_user_id = ? AND skill_id IN (?) ",
		      current_user.id, user.id, counts.map{|count| count[0]})
		    .joins("LEFT JOIN skills ON skill_plus.skill_id = skills.id")
		    .select("skill_plus.id, skills.id AS sid, skills.name AS sname")
	end
end
