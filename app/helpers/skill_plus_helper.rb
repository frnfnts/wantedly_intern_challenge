module SkillPlusHelper
  def is_plused?(skill_id, tagged_user_id, by_user_id)
    not SkillPlu.find_by(by_user_id: by_user_id, tagged_user_id: tagged_user_id, skill_id: skill_id).nil?
  end
end
