require 'test_helper'

class SkillPlusControllerTest < ActionController::TestCase
  setup do
    @skill_plu = skill_plus(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:skill_plus)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create skill_plu" do
    assert_difference('SkillPlu.count') do
      post :create, skill_plu: { by_user_id: @skill_plu.by_user_id, skill_id: @skill_plu.skill_id, tagged_user_id: @skill_plu.tagged_user_id }
    end

    assert_redirected_to skill_plu_path(assigns(:skill_plu))
  end

  test "should show skill_plu" do
    get :show, id: @skill_plu
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @skill_plu
    assert_response :success
  end

  test "should update skill_plu" do
    patch :update, id: @skill_plu, skill_plu: { by_user_id: @skill_plu.by_user_id, skill_id: @skill_plu.skill_id, tagged_user_id: @skill_plu.tagged_user_id }
    assert_redirected_to skill_plu_path(assigns(:skill_plu))
  end

  test "should destroy skill_plu" do
    assert_difference('SkillPlu.count', -1) do
      delete :destroy, id: @skill_plu
    end

    assert_redirected_to skill_plus_path
  end
end
