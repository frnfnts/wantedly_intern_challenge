require 'test_helper'

class TaggedSkillsControllerTest < ActionController::TestCase
  setup do
    @tagged_skill = tagged_skills(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tagged_skills)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tagged_skill" do
    assert_difference('TaggedSkill.count') do
      post :create, tagged_skill: { skill_id: @tagged_skill.skill_id, state: @tagged_skill.state, user_id: @tagged_skill.user_id }
    end

    assert_redirected_to tagged_skill_path(assigns(:tagged_skill))
  end

  test "should show tagged_skill" do
    get :show, id: @tagged_skill
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tagged_skill
    assert_response :success
  end

  test "should update tagged_skill" do
    patch :update, id: @tagged_skill, tagged_skill: { skill_id: @tagged_skill.skill_id, state: @tagged_skill.state, user_id: @tagged_skill.user_id }
    assert_redirected_to tagged_skill_path(assigns(:tagged_skill))
  end

  test "should destroy tagged_skill" do
    assert_difference('TaggedSkill.count', -1) do
      delete :destroy, id: @tagged_skill
    end

    assert_redirected_to tagged_skills_path
  end
end
